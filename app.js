const express = require("express");

// call file router.js
const router = require("./src/routers/router");

const cookieParser = require('cookie-parser');
const session = require('express-session');
const flash = require('express-flash');
const passport = require('./libs/passport');

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.get("/", (req, res) => {
    res.send("Server up!");
})

app.use(cookieParser());
app.use(flash());
app.use(session({
    secrect: 'secretkey',
    resave: false,
    saveUnintialized: false
}));

// Konfigurasi router.
app.use("/", router);

app.listen(port, () => {
    console.log(`Server up on server ${port}!`);
});