const express = require("express");
const users = express.Router();
const fs = require('fs');

users.get("/", (req, res) => {
    fs.readFile("./src/database/user.json", "utf8", (err, data) => {
            if (err) {
            res.status(500).json({
                status: err.code,
                message : err.message
            });
            }
            res.status(200).json(JSON.parse(data));
        });
})

users.get("/list", (req, res) => {
    res.status(200).json(users);
})

module.exports = users;