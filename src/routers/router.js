const express = require("express");
const router = express.Router();
const validate_login = require("../middlewares/login");
const registrasi = require("./registrasi");
const users = require("./user");
const api = require("./api");

router.use("/api", api);

router.get("/", (request, response) =>{
    response.render('index.ejs');
});

//page gameplay
router.get("/gameplay", (request, response) => {
    response.render('gameplay.ejs');
});

//validasi login
router.post("/user", [validate_login], (req,res)=>{
    res.status(200).json(req.body)
})


// json
router.get("/json", (request, response) => {
    const data = {
        "email": "nadia@binar.com",
        "password": "12345qwerty"
    }
    response.status(200).json(data);
});

router.get("/users", users);
router.get("/registrasi", registrasi);

module.exports = router;