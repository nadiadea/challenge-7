const express = require("express");
const authController = require("./src/controllers/authController");
const auth = express.Router();

// ini untuk section endpoint auth;
auth.get('/login', authController.index);
auth.post("/login", authController.login);
auth.get("/logout", authController.logout);

module.exports = auth;
